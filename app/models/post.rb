class Post < ActiveRecord::Base
	validates_presence_of :title
	validates_length_of :body, :in => 10..400,:message =>"longitud no valida"
	has_many :comments
	belongs_to :user
end
