class Comment < ActiveRecord::Base
	validates_presence_of :title
	validates_length_of :body, :in => 20..400,:message =>"longitud no valida"
	belongs_to :post
end
